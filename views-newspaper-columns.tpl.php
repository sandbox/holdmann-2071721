<?php
/**
 * @file views-view-newspaper-columns.tpl.php
 * Flows a list view output into a configurable number of newspaper-styled columns.
 *
 * @ingroup views_templates
 */
?>
<?php foreach ($rows as $id => $row): ?>
  <?php if (in_array($id, $column_open_tag)): ?>
    <div class="view-newspaper-columns view-newspaper-column-<?php print (array_search($id, $column_open_tag) + 1); ?> grid_<?php print $no_columns; ?>">
  <?php endif; ?>

  <?php if (!empty($title) && in_array($id, $group_title_display)): ?>
    <h3><?php print $title; ?></h3>
  <?php endif; ?>

  <div class="<?php print $classes_array[$id]; ?>">
    <?php print $row; ?>
  </div>

  <?php if (in_array($id, $column_close_tag)): ?>
    </div>
  <?php endif; ?>
<?php endforeach; ?>