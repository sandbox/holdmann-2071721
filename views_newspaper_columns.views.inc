<?php
/**
 * @file views_newspaper_columns.views.inc
 * Flow a list view output into a configurable number of newspaper-styled columns.
 */

/**
 * Implementation of hook_views_plugins().
 */
function views_newspaper_columns_views_plugins() {
  return array(
    'style' => array(
      'newspaper_columns' => array(
        'title' => t('Newspaper-styled columns'),
        'help' => t('Flow a list view output into a configurable number of newspaper-styled columns.'),
        'handler' => 'views_columns_plugin_style_newspaper_columns',
        //'parent' => 'table',
        'uses row plugin' => TRUE,
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
        'theme' => 'views_newspaper_columns',
        'even empty' => TRUE,
      ),
    ),
  );
}