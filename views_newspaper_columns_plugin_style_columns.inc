<?php
/**
 * @file
 * Contains the views columns plugin.
 */

/**
 * Style plugin that divides the output into a configurable number of columns.
 *
 * @ingroup views_style_plugins
 */
class views_columns_plugin_style_newspaper_columns extends views_plugin_style {
  function option_definition() {
    $options = parent::option_definition();
    $options['views_newspaper_columns_no_columns'] = array('default' => 2);
    //$options['views_columns_sort_order'] = array('default' => 'vertical');
    return $options;
  }

  /**
   * Render the given style.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['views_newspaper_columns_no_columns'] = array(
      '#title' => t("The number of columns"),
      '#description' => t('The number of columns to show.'),
      '#type' => 'textfield',
      '#size' => '2',
      '#default_value' => $this->options['views_newspaper_columns_no_columns'],
    );
  }
}
